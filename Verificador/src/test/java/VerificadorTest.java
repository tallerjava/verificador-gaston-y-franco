import org.junit.Test;

public class VerificadorTest {
    
    public VerificadorTest(){
    }

    @Test(expected = IllegalArgumentException.class)
    public void verificarPass_valorNull_IllegalArgumentException() throws PasswordInvalidoException{
        Verificador.verificarPass(null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void verificarPass_StringVacio_IllegalArgumentException() throws PasswordInvalidoException{
        Verificador.verificarPass("");
    }
    
    @Test(expected = PasswordInvalidoException.class)
    public void verficarPass_LongitudMenorALaPermitida_PasswordInvalidoException() throws PasswordInvalidoException{
        Verificador.verificarPass("Prueba1");
    }
    
    @Test(expected = PasswordInvalidoException.class)
    public void verficarPass_LongitudMayorALaPermitida_PasswordInvalidoException() throws PasswordInvalidoException{
        Verificador.verificarPass("pruebaCon21Caracteres");
    }
    
    @Test
    public void verficarPass_LongitudMenorPermitidaConNumero_void() throws PasswordInvalidoException{
        Verificador.verificarPass("Crtbvd7h");
    }
    
    @Test
    public void verficarPass_LongitudMayorPermitidaConNumero_void() throws PasswordInvalidoException{
        Verificador.verificarPass("pruebaDe20Caracteres");
    }
    
    @Test(expected = PasswordInvalidoException.class)
    public void verficarPass_LongitudMenorPermitidaSinNumero_PasswordInvalidoException() throws PasswordInvalidoException{
        Verificador.verificarPass("ErvrW#+$");
    }
    
    @Test(expected = PasswordInvalidoException.class)
    public void verficarPass_LongitudMayorPermitidaSinNumero_PasswordInvalidoException() throws PasswordInvalidoException{
        Verificador.verificarPass("ErvrW#+$ErvrW#+$Ervr");
    }

}
