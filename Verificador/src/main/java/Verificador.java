public class Verificador {
       
    public static void verificarPass(String password) throws PasswordInvalidoException{
        
        if(password == null){

            throw new IllegalArgumentException("Argumento invalido. El password debe ser de tipo String, no un valor null");  
            
        }
        
        if(password.equals("")){
            
            throw new IllegalArgumentException("Argumento invalido. El password no puede ser un String vacio");
            
        }
        
        if(password.length() < 8){
            
            throw new PasswordInvalidoException("Argumento invalido. El password ingresado debe contener como minimo 8 caracteres");
            
        }
        
        if(password.length() > 20){
            
            throw new PasswordInvalidoException("Argumento invalido. El password ingresado debe contener como maximo 20 caracteres");
            
        }
        
        if(!password.matches(".*[0-9].*")){
            
            throw new PasswordInvalidoException("Argumento invalido. El password ingresado debe contener al menos un numero");
        
        }
        
    }
    
}