public class PasswordInvalidoException extends Exception {
       
    public PasswordInvalidoException(String mensajeExcepcion) {
        
        super(mensajeExcepcion);
        
    }
   
}
